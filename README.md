# Laporan Praktikum 2 Pemrograman Platform Khusus
# XML dan JSON


Nama	        : Erni Kurnia Putri
<br /> NIM	    : 222011792
<br /> Kelas	: 3SI3
<br /> No       : 29

<br /> Project jwt-app
<br />
1.	Membuat project
<br />![Img 1](Screenshoot/jwt-app 1.jpeg)
<br />Pembuatan project dilakukan menggunakan command prompt dengan bantuan composer
2.	Membuat database
<br />![Img 2](Screenshoot/jwt-app 2.jpeg)
<br />Database dibuat dengan nama jsonwebtoken
3.	Membuat tabel dengan fitur migration
<br />![Img 3](Screenshoot/jwt-app 3a.jpeg)
<br />![Img 4](Screenshoot/jwt-app 3b.jpeg)
<br />![Img 5](Screenshoot/jwt-app 3c.jpeg)
<br />Dengan perintah tersebut, pada database akan tergenerate sebuah tabel bernama migration yang berisi kolom id, email, dan password
4.	Instalasi JSON Web Token (JWT)
<br />![Img 6](Screenshoot/jwt-app 4a.jpeg)
<br />![Img 7](Screenshoot/jwt-app 4b.jpeg)
<br />Konfigurasi token dilakukan untuk mempertahankan autorisasi pengaksesan akun
5.	Membuat file model
<br />![Img 8](Screenshoot/jwt-app 5a.jpeg)
<br />![Img 9](Screenshoot/jwt-app 5b.jpeg)
6.	Membuat file controller Register
<br />![Img 10](Screenshoot/jwt-app 6a.jpeg)
<br />![Img 11](Screenshoot/jwt-app 6b.jpeg)
<br />Controller register dibuat untuk pendaftaran akun awal yang berisi email, password, dan konfirmasi password
7.	Membuat file controller Login
<br />![Img 12](Screenshoot/jwt-app 7a.jpeg)
<br />![Img 13](Screenshoot/jwt-app 7b.jpeg)
<br />Controller login dibuat untuk melakukan login pada akun terdaftar. Login akan menghasilkan token unik sebagai identitas akun
8.	Membuat file controller Me
<br />![Img 14](Screenshoot/jwt-app 8a.jpeg)
<br />![Img 15](Screenshoot/jwt-app 8b.jpeg)
<br />Controller me dibuat untuk mengakses akun ‘milik saya’ sesuai token yang telah tergenerate dari proses login
9.	Membuat file filter Auth
<br />![Img 16](Screenshoot/jwt-app 9a.jpeg)
<br />![Img 17](Screenshoot/jwt-app 9b.jpeg)
<br />![Img 18](Screenshoot/jwt-app 9c.jpeg)
<br />Filter dibuat untuk melakukan validasi dan mengeluarkan pesan error
<br />![Img 19](Screenshoot/jwt-app 9d.jpeg)
<br />Routes dibuat untuk memanggil fungsi-fungsi di controller sehingga dapat ditampilkan pada browser
10.	Membuat file filter CORS
<br />![Img 20](Screenshoot/jwt-app 10a.jpeg)
<br />![Img 21](Screenshoot/jwt-app 10b.jpeg)
<br />![Img 22](Screenshoot/jwt-app 10c.jpeg)
11.	Testing register
<br />![Img 23](Screenshoot/jwt-app 11a.jpeg)
<br />Pada testing fungsi register, status menunjukkan 201 Created sehingga terbukti bahwa akun sudah berhasil didaftarkan. Hal ini dapat terlihat pada database
<br />![Img 24](Screenshoot/jwt-app 11b.jpeg)
<br />Dari database, terlihat bahwa password sudah berhasil dihash
12.	Testing login
<br />![Img 25](Screenshoot/jwt-app 12.jpeg)
<br />Pada testing login, status menunjukkan 200 OK sehingga user berhasil login menggunakan email dan password yang sudah dimasukkan. Selanjutnya, token akan digenerate secara otomatis pada output sebagai identitas akun secara unik sehingga dapat menjadi alat autorisasi akun
13.	Testing me
<br />![Img 26](Screenshoot/jwt-app 13.jpeg)
<br />Pada testing me menggunakan token, output berhasil menampilkan informasi akun yang sedang login. Apabila token salah maka output akan menunjukkan ‘Invalid token’
14.	Access Protected Resources
<br />![Img 27](Screenshoot/jwt-app 14.jpeg)
<br />Untuk memastikan hanya user tertentu yang memiliki hak akses menggunakan akun, maka pada testing home (default browser), user harus memasukkan token unik dari akun yang dimiliki. Apabila token tidak sesuai maka output tidak berhasil ditambilkan dan akan muncul pesan error.
